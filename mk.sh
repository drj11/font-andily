plaq () {
plak --"$2" --line-space="$1" --foreground=eeeeee --background=aa2244 --output-format=svg *.otf |
  tee andily-"$2"-plaque.svg |
  rsvg-convert |
  kitty icat
}

plaq -80 lower

plaq -80 upper
