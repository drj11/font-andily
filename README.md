# Andily

A digitisation of Lydian (Warren Chappell, 1939-01-11)
from the sample at the USPO.

![Lower case alphabet in the font Andily](andily-lower-plaque.svg)
![Upper case alphabet in the font Andily](andily-upper-plaque.svg)


## Design notes

sample required a 0.8 degree rotation to make it level.

Capital height equals ascender height.
Later drawing suggests this might not be quite true.
With caps height set to 700, most of the ascenders seem to top at about 688;
it's a small difference, but consistent enough to not be a mistake.

- x-height is measured at 67.4% of capital height.
- Glyphs Cap Height: 700 (default)
- x-Height: 472


## Exporting images to Glyphs

Export image from Inkscape at 72 phys (advanced options)
1000 pixel image then ranges from descender (-200) to ascender (800)

The baseline to cap-height is 48.70 px in Inkscape.
That should be 700 exported pixels.

A properly aligned FDU pixel box is 69.577 Inkscape pixels,
starting 13.915 Inkscape pixels below the baseline.

(if you add a stroke, the box becomes bigger, so probably better to use the
unstroked box)

That seems to import glyphs very nicely.


## Metrics

Having drawn the alphabet, I measured some metrics:

Capital, vertical thick: B,110,112,D,111,111,E,112,112,F,110,113,H,108,113,111,115,107,108,J,107,105,K,104,105,L,111,109,P,111,112,R,109,112,T,106,110,U,109,109,110,108

Most vertical strokes have some _reverse entasis_ (a gentle concavity).
This measures about 6 FDU in horizontal deflection, which
is approximately 0.9% of cap height.

Capital Cross-stroke (A, E, H):
A,65,E,61,65,63,F,62,64,H,72,L,66,T,59,54

Total variation is 18, which is about 2.5%; but there is also variation within a single stroke.


## Name

Andily is an anagram of Lydian.

# END
